<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Krs;

/**
 * FilterKrs represents the model behind the search form of `app\models\Krs`.
 */
class FilterKrs extends Krs
{
    /**
     * {@inheritdoc}
     */
    public $nim;
    public $nama;
    public $nama_matkul;
    public $kode_kelas;
    public $nama_kelas;

    public function rules()
    {
        return [
            [['nim'], 'integer'],
            [['nama', 'nama_kelas', 'nama_matkul', 'kode_kelas'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Krs::find()->joinWith(['kelas', 'mahasiswa', 'matakuliah']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['nim'] = [
            'asc' => ['mahasiswa.nim' => SORT_ASC],
            'desc' => ['mahasiswa.nim' => SORT_DESC]
        ];

         $dataProvider->sort->attributes['nama'] = [
            'asc' => ['mahasiswa.nama' => SORT_ASC],
            'desc' => ['mahasiswa.nama' => SORT_DESC]
        ];

         $dataProvider->sort->attributes['nama_matkul'] = [
            'asc' => ['matakuliah.nama_matkul' => SORT_ASC],
            'desc' => ['matakuliah.nama_matkul' => SORT_DESC]
        ];

         $dataProvider->sort->attributes['kode_kelas'] = [
            'asc' => ['kelas.kode_kelas' => SORT_ASC],
            'desc' => ['kelas.kode_kelas' => SORT_DESC]
        ];

        $dataProvider->sort->attributes['nama_kelas'] = [
            'asc' => ['kelas.nama_kelas' => SORT_ASC],
            'desc' => ['kelas.nama_kelas' => SORT_DESC]
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }



        // grid filtering conditions
        $query->andFilterWhere([
            'mahasiswa.nim'=>$this->nim
        ]);
        $query->andFilterWhere([
            'like', 'mahasiswa.nama',$this->nama
        ]);
        $query->andFilterWhere([
            'like', 'matakuliah.nama_matkul',$this->nama_matkul
        ]);
        $query->andFilterWhere([
            'like', 'kelas.nama_kelas',$this->nama_kelas
        ]);
        $query->andFilterWhere([
            'like', 'kelas.kode_kelas',$this->kode_kelas
        ]);
        return $dataProvider;
    }
}
