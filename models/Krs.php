<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "krs".
 *
 * @property int $id
 * @property int $id_mahasiswa
 * @property int $id_matakuliah
 * @property int|null $id_kelas
 *
 * @property Kelas $kelas
 * @property Mahasiswa $mahasiswa
 * @property Matakuliah $matakuliah
 */
class Krs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'krs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_mahasiswa', 'id_matakuliah'], 'required'],
            [['id_mahasiswa', 'id_matakuliah', 'id_kelas'], 'integer'],
            [['id_mahasiswa'], 'exist', 'skipOnError' => true, 'targetClass' => Mahasiswa::class, 'targetAttribute' => ['id_mahasiswa' => 'id']],
            [['id_matakuliah'], 'exist', 'skipOnError' => true, 'targetClass' => Matakuliah::class, 'targetAttribute' => ['id_matakuliah' => 'id']],
            [['id_kelas'], 'exist', 'skipOnError' => true, 'targetClass' => Kelas::class, 'targetAttribute' => ['id_kelas' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_mahasiswa' => 'Mahasiswa',
            'id_matakuliah' => 'Matakuliah',
            'id_kelas' => 'Kelas',
        ];
    }

    /**
     * Gets query for [[Kelas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKelas()
    {
        return $this->hasOne(Kelas::class, ['id' => 'id_kelas']);
    }

    /**
     * Gets query for [[Mahasiswa]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMahasiswa()
    {
        return $this->hasOne(Mahasiswa::class, ['id' => 'id_mahasiswa']);
    }

    /**
     * Gets query for [[Matakuliah]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMatakuliah()
    {
        return $this->hasOne(Matakuliah::class, ['id' => 'id_matakuliah']);
    }
}
