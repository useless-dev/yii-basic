<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kelas".
 *
 * @property int $id
 * @property string $kode_kelas
 * @property string $nama_kelas
 *
 * @property Krs[] $krs
 */
class Kelas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kelas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode_kelas', 'nama_kelas'], 'required'],
            [['kode_kelas'], 'string', 'max' => 5],
            [['nama_kelas'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kode_kelas' => 'Kode Kelas',
            'nama_kelas' => 'Nama Kelas',
        ];
    }

    /**
     * Gets query for [[Krs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKrs()
    {
        return $this->hasMany(Krs::class, ['id_kelas' => 'id']);
    }
}
