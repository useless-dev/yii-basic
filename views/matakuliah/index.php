<?php

use app\models\Matakuliah;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\FilterMatakuliah $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Matakuliahs';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">
        <p>
            <?= Html::a('Create Matakuliah', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
        <div class="card-box">
           

            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?= GridView::widget
                ([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        'nama_matkul',
                        [
                            'class' => ActionColumn::className(),
                            'urlCreator' => function ($action, Matakuliah $model, $key, $index, $column) {
                                return Url::toRoute([$action, 'id' => $model->id]);
                             }
                        ],
                    ],
                ]); 
            ?>

        </div>
    </div>
    
</div>

   