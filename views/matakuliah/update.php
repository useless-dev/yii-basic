<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Matakuliah $model */

$this->title = 'Update Matakuliah: ' . $model->nama_matkul;
$this->params['breadcrumbs'][] = ['label' => 'Matakuliahs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama_matkul, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>


