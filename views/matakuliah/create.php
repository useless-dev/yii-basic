<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Matakuliah $model */

$this->title = 'Create Matakuliah';
$this->params['breadcrumbs'][] = ['label' => 'Matakuliahs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>


