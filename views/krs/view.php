<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Krs $model */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Krs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="row">
    <div class="col-md-12">
        <p>
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>
        <div class="card-box">
             <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    [
                        'attribute' => 'id_mahasiswa',
                        'value' => function ($model)
                        {
                            return $model->mahasiswa->nama;
                        }
                    ],
                    [
                        'attribute' => 'id_matakuliah',
                        'value' => function ($model)
                        {
                            return $model->matakuliah->nama_matkul;
                        }
                    ],
                    [
                        'attribute' => 'id_kelas',
                        'value' => function ($model)
                        {
                            return $model->kelas->nama_kelas;
                        }
                    ],
                ],
            ]) ?>

        </div>
    </div>
</div>

    

   
