<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Krs $model */

$this->title = 'Create Krs';
$this->params['breadcrumbs'][] = ['label' => 'Krs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

