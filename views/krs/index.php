<?php

use app\models\Krs;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\FilterKrs $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Krs';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">
          <p>
                <?= Html::a('Create Krs', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
        <div class="card-box">
                     

            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'nim', 
                        'value' => function($model){
                            return $model->mahasiswa->nim;
                        }
                    ],
                   
                    [
                        'attribute' => 'nama', 
                        'value' => function($model){
                            return $model->mahasiswa->nama;
                        }
                    ],

                    [
                        'attribute' => 'kode_kelas', 
                        'value' => function($model){
                            return $model->kelas->kode_kelas;
                        }
                    ],

                    [
                        'attribute' => 'nama_kelas', 
                        'value' => function($model){
                            return $model->kelas->nama_kelas;
                        }
                    ],
                    [
                        'attribute' => 'nama_matkul', 
                        'value' => function($model){
                            return $model->matakuliah->nama_matkul;
                        }
                    ],

                    [
                        'class' => ActionColumn::className(),
                        'urlCreator' => function ($action, Krs $model, $key, $index, $column) {
                            return Url::toRoute([$action, 'id' => $model->id]);
                         }
                    ],
                ],
            ]); ?>


        </div>
    </div>
</div>


