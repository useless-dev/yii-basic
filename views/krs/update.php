<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Krs $model */

$this->title = 'Update Krs: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Krs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>


