<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Matakuliah;
use app\models\Mahasiswa;
use app\models\Kelas;

$matakuliah=ArrayHelper::map(Matakuliah::find()->all(),'id', 'nama_matkul');
$mahasiswa=ArrayHelper::map(Mahasiswa::find()->all(), 'id', function($data){
    return $data['nim'].' - '.$data['nama'];
});
$kelas=ArrayHelper::map(Kelas::find()->all(), 'id', function($data){
    return $data['kode_kelas'].' - '.$data['nama_kelas'];
});

?>

<div class="row">
    <div class="col-md-12">
        <div class="card-box">
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'id_mahasiswa')->dropDownList($mahasiswa) ?>

            <?= $form->field($model, 'id_matakuliah')->dropDownList($matakuliah) ?>

            <?= $form->field($model, 'id_kelas')->dropDownList($kelas) ?>

            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

   
