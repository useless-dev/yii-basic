<?php

    use app\models\Mahasiswa;
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\grid\ActionColumn;
    use yii\grid\GridView;

    /** @var yii\web\View $this */
    /** @var app\models\FilterMahasiswa $searchModel */
    /** @var yii\data\ActiveDataProvider $dataProvider */

    $this->title = 'Mahasiswas';
    $this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">
            
        <p>
            <?= Html::a('Create Mahasiswa', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
        <div class="card-box">
            <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        'nim',
                        'nama',
                        [
                            'class' => ActionColumn::className(),
                            // 'urlCreator' => function ($action, Mahasiswa $model, $key, $index, $column) {
                            //     return Url::toRoute([$action, 'id' => $model->id]);
                            //  }
                            'buttons' => [
                                'view' => function ($url, $model)
                                {
                                    return Html::a('<i class="fa fa-eye"></i>', $url, ['class' => 'btn btn-icon waves-effect btn-default waves-light']);
                                },
                                'update' => function ($value='')
                                {
                                    return null;
                                },
                                'delete' => function($value='')
                                {
                                    return null;
                                }
                            ]

                        ],
                    ],
                ]);
            ?>
        </div>
    </div>
    
</div>
