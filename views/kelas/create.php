<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Kelas $model */

$this->title = 'Create Kelas';
$this->params['breadcrumbs'][] = ['label' => 'Kelas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
