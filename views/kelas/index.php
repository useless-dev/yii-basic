<?php

use app\models\Kelas;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\FilterKelas $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Kelas';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">
        <p>
            <?= Html::a('Create Kelas', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
        <div class="card-box">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'kode_kelas',
                    'nama_kelas',
                    [
                        'class' => ActionColumn::className(),
                        'urlCreator' => function ($action, Kelas $model, $key, $index, $column) {
                            return Url::toRoute([$action, 'id' => $model->id]);
                         }
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>




