<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Kelas $model */

$this->title = 'Update Kelas: ' . $model->nama_kelas;
$this->params['breadcrumbs'][] = ['label' => 'Kelas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama_kelas, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

